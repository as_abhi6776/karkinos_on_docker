# Karkinos_ON_Docker
This is Karkinos_ON_Docker. In this repository i have just added dockerfile to run Karkinose on docker environment. You can use this dockerfile to deploy karkinose in your dev environment. We are working for stable production release.

## Usage
To run this automated bash script please see following instruction.

### Build and Run

```bash
git clone https://gitlab.com/as_abhi6776/karkinos_on_docker.git
cd karkinos_on_docker
docker build -t karkinos-docker:tag
docker run -it -d -p 8888:8888 -p 5555:5555 -p 5557:5557 -p 5556:5556 karkinos-docker:tag
```

### run with prebuild image

```bash
docker run -it -d -p 8888:8888 -p 5555:5555 -p 5557:5557 -p 5556:5556 asabhi6776/karkinos:latest
```

## Update
Image is working fine, but we will keep on updating the docker image so please before running please check for stable update.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://mit-license.org/)

## Note
Original code is written by [helich0pper/Karkinos](https://github.com/helich0pper/Karkinos/). Only dockerfile is added by me. So please support [helich0pper/Karkinos](https://github.com/helich0pper/Karkinos/).

# Thanks
